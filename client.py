# 				WORK IN PROGRESS
# Import required libraries
import requests
import datetime
# import time
import json
import hashlib
import hmac
import random
import string
import configparser  # pip install configparser
from Crypto.Cipher import AES
# from Crypto.Util                         import Counter
# from Crypto                              import Random
from base64 import b64encode  # ,b64decode

# Read the configuration
config = configparser.ConfigParser()
config.read('.env')


# PAYTHEM module definition
class PTN_API_v2:
    # These parameters are read from the .env file.
    def __init__(self):
        self.url = config['PAYTHEM']['url']  # Change this value in .env to reflect the current environment.
        self.content = {
            'API_VERSION': '2.0.5',
            'SERVER_URI': self.url,
            'ENCRYPT_RESPONSE': bool(0),
            'SERVER_DEBUG': bool(0),
            'FAULTY_PROXY': bool(0),
            'SOURCE_IP': config['PAYTHEM']['host_ip'],  # Change this to the current production environment's public IP
            'USERNAME': config['PAYTHEM']['username'],  # As provided by PAYTHEM
            'PASSWORD': config['PAYTHEM']['password'],  # As provided by PAYTHEM
            'PUBLIC_KEY': config['PAYTHEM']['publicKey'],  # As provided by PAYTHEM
            'SERVER_TIMEZONE': config['PAYTHEM']['timezone'],  # Change this to your current time zone
            'PARAMETERS': None
        }
        self.data = {
            'PUBLIC_KEY': config['PAYTHEM']['publicKey']
        }
        self.headers = {
            'X-Public-Key': config['PAYTHEM']['publicKey'],
            'X-Sourceip': config['PAYTHEM']['host_ip'],
        }

    # Encryption function
    def encrypt(self, plaintext):
        pad = plaintext + (16 - len(plaintext) % 16) * chr(16 - len(plaintext) % 16)
        pad = str.encode(pad)
        key = str.encode(config['PAYTHEM']['privateKey'])
        iv = str.encode(self.data['ZAPI'])
        aes = AES.new(key, AES.MODE_CBC, iv)
        cipher_text = b64encode(aes.encrypt(pad))
        return cipher_text

    # HMAC generation
    def hmac_sha256(self, msg):
        return hmac.new(key=config['PAYTHEM']['privateKey'].encode(), msg=json.dumps(msg).encode(),
                        digestmod=hashlib.sha256).hexdigest()

    def genIV(self):
        return ''.join(random.choice(string.ascii_lowercase) for i in range(16))

    def doCall(self):
        self.content['SERVER_TIMESTAMP'] = str(datetime.datetime.now())
        self.data['ZAPI'] = self.genIV()
        self.data['CONTENT'] = self.encrypt(json.dumps(self.content))
        self.data['HASH_STUB'] = self.genIV()
        self.headers['X-Hash'] = self.hmac_sha256(self.content)
        return requests.post(url=self.url, data=self.data, headers=self.headers)

    #        print(self.content)
    #        print(self.data)
    #        print(self.headers)
    #        start                            = time.time()
    #        r                                = requests.post(url=self.url, data=self.data, headers=self.headers)
    #        end                              = time.time()
    #        print("Call took ", end - start, " seconds")
    #        return r
    def get_OEMList(self):
        self.content['FUNCTION'] = 'get_OEMList'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_BrandList(self):
        self.content['FUNCTION'] = 'get_OEMList'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_ProductList(self):
        self.content['FUNCTION'] = 'get_ProductList'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_ProductAvailability(self, prodID):
        self.content['FUNCTION'] = 'get_ProductAvailability'
        self.content['PARAMETERS'] = {
            'PRODUCT_ID': prodID
        }
        return self.doCall()

    def get_AccountBalance(self):
        self.content['FUNCTION'] = 'get_AccountBalance'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_Vouchers(self, prodID, qty):
        self.content['FUNCTION'] = 'get_Vouchers'
        self.content['PARAMETERS'] = {
            'PRODUCT_ID': prodID,
            'QUANTITY': qty,
        }
        return self.doCall()

    def get_FinancialTransaction_ByDateRange(self, dtFrom, dtTo):
        self.content['FUNCTION'] = 'get_FinancialTransaction_ByDateRange'
        self.content['PARAMETERS'] = {
            'FROM_DATE': dtFrom,
            'TO_DATE': dtTo
        }
        return self.doCall()

    def get_TransactionByReferenceId(self, referenceId):
        self.content['FUNCTION'] = 'get_TransactionByReferenceId'
        self.content['PARAMETERS'] = {
            'REFERENCE_ID': referenceId
        }
        return self.doCall()

    def get_AllProductAvailability(self):
        self.content['FUNCTION'] = 'get_AllProductAvailability'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_LastSale(self):
        self.content['FUNCTION'] = 'get_LastSale'
        self.content['PARAMETERS'] = None
        return self.doCall()
        return self.doCall()

    def get_ProductFormats(self):
        self.content['FUNCTION'] = 'get_ProductFormats'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_ProductInfo(self, productId):
        self.content['FUNCTION'] = 'get_ProductInfo'
        self.content['PARAMETERS'] = {
            'PRODUCT_ID' : productId
        }
        return self.doCall()

    def get_MaxAllowedVouchersPerCall(self):
        self.content['FUNCTION'] = 'get_MaxAllowedVouchersPerCall'
        self.content['PARAMETERS'] = None
        return self.doCall()

    def get_SalesTransaction_ByDateRange(self, dtFrom, dtTo):
        self.content['FUNCTION'] = 'get_SalesTransaction_ByDateRange'
        self.content['PARAMETERS'] = {
            'FROM_DATE': dtFrom,
            'TO_DATE': dtTo
        }
        return self.doCall()


pt = PTN_API_v2()
print(pt.get_OEMList().content)